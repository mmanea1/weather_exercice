from selenium import webdriver
import sys


CHROME_OPTIONS = webdriver.ChromeOptions()
CHROME_OPTIONS.add_argument('--no-sandbox')
CHROME_OPTIONS.add_argument('--disable-extensions')
CHROME_OPTIONS.add_argument('--disable-dev-shm-usage')
CHROME_OPTIONS.add_argument('--window-size=1920,1080')
CHROME_OPTIONS.add_argument('--ignore-certificate-errors')
CHROME_OPTIONS.add_argument("--headless")

# Start a google-chrome browser
try: 
    driver = webdriver.Chrome(chrome_options=CHROME_OPTIONS)
except Exception as e:
    print(f"Failed to to open browser due to error: {e}")
    driver.quit()
    sys.exit()
# Navigate to requested URL

try:
    driver.get("https://www.weerindelft.nl")
except Exception as e:
    print(f"Failed to navigate to the requeste URL")
    driver.quit()
    sys.exit()

# Move to iframe where the xpath temperature is present
try:
    iframe = driver.find_element_by_xpath("//iframe[@name='ifrm_3']")
    driver.switch_to.frame(iframe)
except Exception as e:
    print(f"Failed to move to iframe xpath where temperature is present")
    driver.quit()
    sys.exit()

# Get temperature element
try:
    elem=driver.find_element_by_xpath("//*[@id='ajaxtemp']")
except Exception as e:
    print(f"Failed to get the xpath for temperature")
    driver.quit()
    sys.exit()

# Print temperature rounded to degrees
print(f"{round(float(elem.text[:-2]))} degrees Celsius")

# Quit browser
driver.quit()
