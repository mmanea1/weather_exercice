FROM ubuntu:18.04
COPY HoeWarmIsHetInDelft.py /
RUN apt update && apt install -y python3 && apt install -y python3-pip && pip3 install selenium && apt install -y unzip
RUN apt install -y wget && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list' && apt update && apt install -y google-chrome-stable
RUN CHROMEDRIVER_VERSION=$(wget -4 -q -O - http://chromedriver.storage.googleapis.com/LATEST_RELEASE) && wget -4 -q http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip && mv chromedriver /usr/local/bin/
CMD python3 HoeWarmIsHetInDelft.py
